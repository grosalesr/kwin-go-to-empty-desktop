# KWin Switch to Empty Desktop

KWin Script to switch to an empty desktop.

> This script was developed to be used along with [KWin dynamic workspaces](https://store.kde.org/p/1312691/)

## Installation

### Method 1: From the KDE Store

Go to `System Settings` -> `Window Management` -> `KWin Scripts` -> `Get New Scripts`

Search for 'switch to an empty desktop'

Click the Install button

[Pling Store link](https://www.pling.com/p/2128797/)

### Method 2: From kwinscript file

Download the _.kwinscript_ file, from [here]()

Then go to: `System Settings` -> `Window Management` -> `KWin Scripts` -> `Install from File`.

### Method 3: From source

* Get the source code

```
git clone https://gitlab.com/grosalesr/kwin-go-to-empty-desktop.git
```

* Install:

```
kpackagetool5 --type=KWin/Script -i kwin-switch-to-empty-desktop
```

* Update:

```
kpackagetool5 --type=KWin/Script -u kwin-switch-to-empty-desktop
```

> **Note:** disable and then enable script from `System Settings` -> `Window Management` -> `KWin Scripts` for changes to take effect.
> Log out and back in for changes to take effect.

uninstall:

```
kpackagetool5 --type=KWin/Script -r kwin-switch-to-empty-desktop
```

After installing the script, it must be also enabled in the System Settings.

## Configuration

The default shortcut is <kbd>Meta</kbd>+<kbd>End</kbd> .

Use `System Settings` -> `Shortcuts` to change it.

## Development

**Resources**

* [Plasma KWin API](https://develop.kde.org/docs/plasma/kwin/api/)
    *  types used:
        * `KWin::WorkspaceWrapper`
        * `KWin::ToplevelWindow`
* [Plasma KWin Scripting documentation](https://develop.kde.org/docs/plasma/kwin/)

**Desktop Scripting Console**

Open the Plasma Desktop Scripting Console using `krunner` and enter:

```
plasma-interactiveconsole --kwin
```

View output/logs using:

> **Notice**: KWin interactive console does not work, [see bug](https://bugs.kde.org/show_bug.cgi?id=445058)

```
journalctl -g "js:" -f
```

### Packaging

If `make` is installed, run `make build` or manually by executing:

```
zip -r kwin-switch-to-empty-desktop.kwinscript contents/ LICENSE metadata.desktop
```
