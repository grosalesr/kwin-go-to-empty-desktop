function getClientsPerDesktop() {
  // return an array with number of clients (windows) per desktop
  // the array index is used to identify the desktop, eg: index 0 -> workspace 1

  // workspace.desktops -> returns the number of desktops (int)
  let desktops = new Array(workspace.desktops).fill(0)
  //console.info("number of desktops: " + desktops.length);

  // get hold of all clients (windows) managed by the window manager
  clients = workspace.clientList();

  // determine how many clients are per desktop
  const onAllDesktops = -1;
  for (let i = 0; i < clients.length; i++) {
    let client = clients[i];

    if (client.desktop != onAllDesktops) {
      desktops[client.desktop - 1] += 1;
    }
  }

  for (let i = 0; i < desktops.length; i++) {
    let desktop = i + 1;
    let numberOfClients = desktops[i];
    //console.info("desktop " + desktop + ": " + numberOfClients + " clients");
  }

  return desktops;
}

function switchToEmptyDesktop() {
  let currentDesktop = workspace.currentDesktop;
  let clientsPerDesktop = getClientsPerDesktop();
  let totalDesktops = clientsPerDesktop.length;

  //console.info("currentDesktop: " + currentDesktop);
  //console.info("clientsPerDesktop: " + clientsPerDesktop);
  //console.info("totalDesktops: " + totalDesktops);

  for (let i = 0; i < totalDesktops; i++) {
    let desktopClients = clientsPerDesktop[i]

    if (desktopClients == 0) {
      console.info("empty workspace position: " + i)
      let delta = (totalDesktops - currentDesktop);
      workspace.currentDesktop = (currentDesktop + delta);
      return;
    }
  }

  return;
}

registerShortcut('SwitchToEmptyDesktop', 'Switch to Empty Desktop', 'Meta+End', function () { switchToEmptyDesktop(); });
